package com.utm.smartserver.service.impl;

import com.utm.smartserver.dao.EmployeeDAO;
import com.utm.smartserver.exception.OrderResourceNotFoundException;
import com.utm.smartserver.model.Employee;
import com.utm.smartserver.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    final static List<String> employeeFields = new LinkedList<String>(
            Arrays.asList("department_id", "salary", "id"));

    @Autowired
    EmployeeDAO employeeDAO;

    public Employee findById(int id) {
        return employeeDAO.findById(id);
    }

    public void saveEmployee(Employee employee) {
        employeeDAO.saveEmployee(employee);
    }

    public void updateEmployee(Employee employee) {
        employeeDAO.updateEmployee(employee);
    }

    public void deleteEmployeeById(int id) {
        employeeDAO.deleteEmployeeById(id);
    }

    public List<Employee> findAllEmployees(Integer limit, Integer departmentId, String firstName,
                                           Integer maxSalary, String orderBy) {
        if (orderBy != null) {
            if (orderBy.equalsIgnoreCase(employeeFields.get(0))) {
                orderBy = "departmentId";
            } else if (!employeeFields.contains(orderBy)) {
                throw new OrderResourceNotFoundException(orderBy);
            }
        }
        return employeeDAO.findAllEmployees(limit, departmentId, firstName, maxSalary, orderBy);
    }
}
