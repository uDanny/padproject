package com.utm.smartserver.service;

import com.utm.smartserver.model.Department;
import com.utm.smartserver.model.Employee;

import java.util.List;


public interface DepartmentService {

    Department findById(int id);

    void saveDepartment(Department department);

    void updateDepartment(Department department);

    void deleteDepartmentById(int id);

    List<Department> findAllDepartments(Integer limit, String location, String name, String orderBy);

}
