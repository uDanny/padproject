package com.utm.smartserver.service;

import com.utm.smartserver.model.Employee;

import java.util.List;


public interface EmployeeService {

    Employee findById(int id);

    void saveEmployee(Employee employee);

    void updateEmployee(Employee employee);

    void deleteEmployeeById(int id);

    List<Employee> findAllEmployees(Integer limit, Integer departmentId, String firstName, Integer maxSalary, String orderBy);

}
