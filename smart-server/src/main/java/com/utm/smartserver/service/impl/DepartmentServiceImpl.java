package com.utm.smartserver.service.impl;

import com.utm.smartserver.dao.DepartmentDAO;
import com.utm.smartserver.dao.EmployeeDAO;
import com.utm.smartserver.exception.OrderResourceNotFoundException;
import com.utm.smartserver.model.Department;
import com.utm.smartserver.model.Employee;
import com.utm.smartserver.service.DepartmentService;
import com.utm.smartserver.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service("departmentService")
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    final static List<String> departmentFields = new LinkedList<String>(
            Arrays.asList("id"));

    @Autowired
    DepartmentDAO departmentDAO;

    @Override
    public Department findById(int id) {
        return departmentDAO.findById(id);
    }

    @Override
    public void saveDepartment(Department department) {
        departmentDAO.saveDepartment(department);
    }

    @Override
    public void updateDepartment(Department department) {
        departmentDAO.updateDepartment(department);
    }

    @Override
    public void deleteDepartmentById(int id) {
        departmentDAO.deleteDepartmentById(id);
    }

    @Override
    public List<Department> findAllDepartments(Integer limit, String location, String name, String orderBy) {
        if (orderBy != null && !orderBy.equalsIgnoreCase(departmentFields.get(0))){
            throw new OrderResourceNotFoundException(orderBy);
        }
        return departmentDAO.findAllDepartments(limit, location, name, orderBy);
    }
}
