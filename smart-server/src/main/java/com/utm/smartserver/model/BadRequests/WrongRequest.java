package com.utm.smartserver.model.BadRequests;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WrongRequest {
    private String text;
    private String request;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WrongRequest wrongRequest = (WrongRequest) o;

        if (text != null ? !text.equals(wrongRequest.text) : wrongRequest.text != null) return false;
        return request != null ? request.equals(wrongRequest.request) : wrongRequest.request == null;
    }

    @Override
    public int hashCode() {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (request != null ? request.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WrongRequest{" +
                "text='" + text + '\'' +
                ", request='" + request + '\'' +
                '}';
    }
}
