package com.utm.smartserver.model.HateoasResources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.utm.smartserver.model.Employee;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class EmployeeResource extends ResourceSupport {

    private Employee employee;

    @JsonCreator
    public EmployeeResource(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
