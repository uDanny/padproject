package com.utm.smartserver.model.HateoasResources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.utm.smartserver.model.Department;
import com.utm.smartserver.model.Employee;
import org.springframework.hateoas.ResourceSupport;

public class DepartmentResource extends ResourceSupport {

    private Department department;

    @JsonCreator
    public DepartmentResource(Department department) {
        this.department = department;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
