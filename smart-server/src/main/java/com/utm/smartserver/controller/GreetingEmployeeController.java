package com.utm.smartserver.controller;

import com.utm.smartserver.model.Employee;
import com.utm.smartserver.model.HateoasResources.EmployeeResource;
import com.utm.smartserver.utils.Headers;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class GreetingEmployeeController {

    @RequestMapping("/employee/greeting")
    public List<HttpEntity<? extends ResourceSupport>> greetingEmployee() {

        List<HttpEntity<? extends ResourceSupport>> responseList = new LinkedList<>();

        Employee employee = getEmployeeExample();
        ResourceSupport ResourceSupport;
        EmployeeResource employeeResource;


        //self
        self(responseList);

        //listAllEmployees
        listAllEmployees(responseList, employee);

        //retrieve an user
        retrieveAnUser(responseList);

        //create an user

        createAnUser(responseList, employee);

        //update an user
        updateAnUser(responseList, employee);

        //delete an user
        deleteAnUser(responseList);

        return responseList;
    }

    private void deleteAnUser(List<HttpEntity<? extends ResourceSupport>> responseList) {
        ResourceSupport ResourceSupport;
        ResourceSupport = new ResourceSupport();
        ResourceSupport.add(linkTo(methodOn(EmployeeController.class)
                .deleteUser(0)).withRel("deleteUser"));
        responseList.add(new ResponseEntity<>(ResourceSupport, HttpStatus.NO_CONTENT));
    }

    private void updateAnUser(List<HttpEntity<? extends ResourceSupport>> responseList, Employee employee) {
        EmployeeResource employeeResource;
        employeeResource = new EmployeeResource(employee);
        employeeResource.add(linkTo(methodOn(EmployeeController.class)
                .updateUser(0, employee)).withRel("updateUser"));
        responseList.add(new ResponseEntity<>(employeeResource, Headers.getContentHeaders(), HttpStatus.OK));
    }

    private void createAnUser(List<HttpEntity<? extends ResourceSupport>> responseList, Employee employee) {
        EmployeeResource employeeResource;
        employeeResource = new EmployeeResource(employee);
        employeeResource.add(linkTo(methodOn(EmployeeController.class)
                .createUser(employee, null)).withRel("createUser"));
        responseList.add(new ResponseEntity<>(employeeResource, Headers.getContentHeaders(), HttpStatus.CREATED));
    }

    private void retrieveAnUser(List<HttpEntity<? extends ResourceSupport>> responseList) {
        ResourceSupport ResourceSupport;
        ResourceSupport = new ResourceSupport();
        ResourceSupport.add(linkTo(methodOn(EmployeeController.class)
                .getUser(0)).withRel("getUser"));
        responseList.add(new ResponseEntity<>(ResourceSupport, Headers.getAcceptHeaders(), HttpStatus.OK));
    }

    private void listAllEmployees(List<HttpEntity<? extends ResourceSupport>> responseList, Employee employee) {
        ResourceSupport ResourceSupport;
        ResourceSupport = new ResourceSupport();
        ResourceSupport.add(linkTo(methodOn(EmployeeController.class)
                .listAllEmployeesByCriteria(5, 1, employee.getFirstName(), 4000, "salary"))
                .withRel("listAllEmployeesByCriteria"));
        responseList.add(new ResponseEntity<>(ResourceSupport, Headers.getAcceptHeaders(), HttpStatus.OK));
    }

    private void self(List<HttpEntity<? extends ResourceSupport>> responseList) {
        ResourceSupport ResourceSupport;
        ResourceSupport = new ResourceSupport();
        ResourceSupport.add(linkTo(methodOn(GreetingEmployeeController.class).greetingEmployee()).withSelfRel());
        responseList.add(new ResponseEntity<>(ResourceSupport, HttpStatus.OK));
    }

    private Employee getEmployeeExample() {
        Employee exampleEmployee = new Employee();
        exampleEmployee.setFirstName("FirstName");
        exampleEmployee.setLastName("LastName");
        exampleEmployee.setSalary(2000);
        exampleEmployee.setJobTitle("DEV");
        return exampleEmployee;
    }
}