package com.utm.smartserver.controller;

import com.utm.smartserver.model.Department;
import com.utm.smartserver.model.HateoasResources.DepartmentResource;
import com.utm.smartserver.utils.Headers;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class GreetingDepartmentController {

    @RequestMapping("/department/greeting")
    public List<HttpEntity<? extends ResourceSupport>> greetingDepartment() {

        List<HttpEntity<? extends ResourceSupport>> responseList = new LinkedList<>();

        Department department = getDepartmentExample();
        ResourceSupport ResourceSupport;
        DepartmentResource departmentResource;


        //self
        self(responseList);

        //listAllDepartments
        listAllDepartments(responseList, department);

        //retrieve a Department
        retrieveADepartment(responseList);

        //create a Department
        createADepartment(responseList, department);

        //update a Department
        updateADepartment(responseList, department);

        //delete a Department
        deleteADepartment(responseList);

        return responseList;
    }

    private void deleteADepartment(List<HttpEntity<? extends ResourceSupport>> responseList) {
        ResourceSupport ResourceSupport;
        ResourceSupport = new ResourceSupport();
        ResourceSupport.add(linkTo(methodOn(DepartmentController.class)
                .deleteDepartment(0)).withRel("deleteDepartment"));
        responseList.add(new ResponseEntity<>(ResourceSupport, HttpStatus.NO_CONTENT));
    }

    private void updateADepartment(List<HttpEntity<? extends ResourceSupport>> responseList, Department department) {
        DepartmentResource departmentResource;
        departmentResource = new DepartmentResource(department);
        departmentResource.add(linkTo(methodOn(DepartmentController.class)
                .updateDepartment(0, department)).withRel("updateDepartment"));
        responseList.add(new ResponseEntity<>(departmentResource, Headers.getContentHeaders(), HttpStatus.OK));
    }

    private void createADepartment(List<HttpEntity<? extends ResourceSupport>> responseList, Department department) {
        DepartmentResource departmentResource;
        departmentResource = new DepartmentResource(department);
        departmentResource.add(linkTo(methodOn(DepartmentController.class)
                .createDepartment(department, null)).withRel("createDepartment"));
        responseList.add(new ResponseEntity<>(departmentResource, Headers.getContentHeaders(), HttpStatus.CREATED));
    }

    private void retrieveADepartment(List<HttpEntity<? extends ResourceSupport>> responseList) {
        ResourceSupport ResourceSupport;
        ResourceSupport = new ResourceSupport();
        ResourceSupport.add(linkTo(methodOn(DepartmentController.class)
                .getDepartment(0)).withRel("getDepartment"));
        responseList.add(new ResponseEntity<>(ResourceSupport, Headers.getAcceptHeaders(), HttpStatus.OK));
    }

    private void listAllDepartments(List<HttpEntity<? extends ResourceSupport>> responseList, Department department) {
        ResourceSupport ResourceSupport;
        ResourceSupport = new ResourceSupport();
        ResourceSupport.add(linkTo(methodOn(DepartmentController.class)
                .listAllDepartmentsByCriteria(5, department.getLocation(), department.getName(), "id"))
                .withRel("listAllDepartmentsByCriteria"));
        responseList.add(new ResponseEntity<>(ResourceSupport, Headers.getAcceptHeaders(), HttpStatus.OK));
    }

    private void self(List<HttpEntity<? extends ResourceSupport>> responseList) {
        ResourceSupport ResourceSupport;
        ResourceSupport = new ResourceSupport();
        ResourceSupport.add(linkTo(methodOn(GreetingDepartmentController.class).greetingDepartment()).withSelfRel());
        responseList.add(new ResponseEntity<>(ResourceSupport, HttpStatus.OK));
    }

    private Department getDepartmentExample() {
        Department exampleDepartment = new Department();
        exampleDepartment.setName("name");
        exampleDepartment.setLocation("location");
        return exampleDepartment;
    }
}