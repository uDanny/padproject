package com.utm.smartserver.controller;

import com.utm.smartserver.model.Employee;
import com.utm.smartserver.model.Employees;
import com.utm.smartserver.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    //-------------------Retrieve All Employees + Criterias--------------------------------------------------------

    @RequestMapping(value = "/employee/", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Employees> listAllEmployeesByCriteria(
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "department_id", required = false) Integer departmentId,
            @RequestParam(value = "contains_firstname", required = false) String firstName,
            @RequestParam(value = "max_salary", required = false) Integer maxSalary,
            @RequestParam(value = "order_by", required = false) String orderBy
    ) {
        List<Employee> employees = employeeService.findAllEmployees(limit, departmentId, firstName, maxSalary, orderBy);
        if (employees.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(new Employees(employees), HttpStatus.OK);
    }

    //-------------------Retrieve Single Employee--------------------------------------------------------

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Employee> getUser(@PathVariable("id") int id) {

        System.out.println("Fetching User with id " + id);
        Employee employee = employeeService.findById(id);
        if (employee == null) {
            System.out.println("employee with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    //-------------------Create an Employee--------------------------------------------------------

    @RequestMapping(value = "/employee/", method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Void> createUser(@RequestBody Employee employee, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Employee " + employee.getFirstName() + employee.getLastName());

        employeeService.saveEmployee(employee);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/employee/{id}").buildAndExpand(employee.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }


    //-------------------Update an Employee--------------------------------------------------------

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.PUT,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Employee> updateUser(@PathVariable("id") int id, @RequestBody Employee employee) {
        System.out.println("Updating Employee " + id);

        Employee currentEmployee = employeeService.findById(id);

        if (currentEmployee == null) {
            System.out.println("Employee with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        currentEmployee.setFirstName(employee.getFirstName());
        currentEmployee.setLastName(employee.getLastName());
        currentEmployee.setDepartmentId(employee.getDepartmentId());
        currentEmployee.setJobTitle(employee.getJobTitle());
        currentEmployee.setSalary(employee.getSalary());


        employeeService.updateEmployee(currentEmployee);
        return new ResponseEntity<>(currentEmployee, HttpStatus.OK);
    }

    //-------------------Delete an Employee--------------------------------------------------------

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Void> deleteUser(@PathVariable("id") int id) {
        System.out.println("Fetching & Deleting Employee with id " + id);

        Employee employee = employeeService.findById(id);
        if (employee == null) {
            System.out.println("Unable to delete. Employee with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        employeeService.deleteEmployeeById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    //------------------- Temp MVC view --------------------------------------------------------
    @RequestMapping(value = "/", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ModelAndView sayHello(Model model) {
        model.addAttribute("message", "Welcome from spring - mvc");
        return new ModelAndView("welcome");
    }
}