package com.utm.smartserver.controller;

import com.utm.smartserver.model.Department;
import com.utm.smartserver.model.Departments;
import com.utm.smartserver.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

//-------------------Retrieve All Departments + Criterias--------------------------------------------------------

    @RequestMapping(value = "/department/", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Departments> listAllDepartmentsByCriteria(
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "location", required = false) String location,
            @RequestParam(value = "contains_name", required = false) String name,
            @RequestParam(value = "order_by", required = false) String orderBy
    ) {
        List<Department> departments = departmentService.findAllDepartments(limit, location, name, orderBy);
        if (departments.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(new Departments(departments), HttpStatus.OK);
    }

    //-------------------Retrieve Single Department--------------------------------------------------------

    @RequestMapping(value = "/department/{id}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Department> getDepartment(@PathVariable("id") int id) {

        System.out.println("Fetching Department with id " + id);
        Department department = departmentService.findById(id);
        if (department == null) {
            System.out.println("department with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(department, HttpStatus.OK);
    }

    //-------------------Create a Department--------------------------------------------------------

    @RequestMapping(value = "/department/", method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Void> createDepartment(@RequestBody Department department, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Department " + department.getName() + department.getLocation());

        departmentService.saveDepartment(department);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/department/{id}").buildAndExpand(department.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    //-------------------Update a Department--------------------------------------------------------

    @RequestMapping(value = "/department/{id}", method = RequestMethod.PUT,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Department> updateDepartment(@PathVariable("id") int id, @RequestBody Department department) {
        System.out.println("Updating Department " + id);

        Department currentDepartment = departmentService.findById(id);

        if (currentDepartment == null) {
            System.out.println("Department with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        currentDepartment.setLocation(department.getLocation());
        currentDepartment.setName(department.getName());

        departmentService.updateDepartment(currentDepartment);
        return new ResponseEntity<>(currentDepartment, HttpStatus.OK);
    }

    //-------------------Delete a Department--------------------------------------------------------

    @RequestMapping(value = "/department/{id}", method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public ResponseEntity<Void> deleteDepartment(@PathVariable("id") int id) {
        System.out.println("Fetching & Deleting Department with id " + id);

        Department department = departmentService.findById(id);
        if (department == null) {
            System.out.println("Unable to delete. Department with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        departmentService.deleteDepartmentById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}