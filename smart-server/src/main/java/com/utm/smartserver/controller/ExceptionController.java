package com.utm.smartserver.controller;

import com.utm.smartserver.exception.OrderResourceNotFoundException;
import com.utm.smartserver.model.BadRequests.WrongRequest;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionController {

    //-------------------Bad Request Uri --------------------------------------------------------
    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<WrongRequest> handleError404(HttpServletRequest request, Exception e) {
        WrongRequest wrongRequest = new WrongRequest();
        wrongRequest.setRequest(request.getRequestURI());
        wrongRequest.setText("wrong request URI");
        return new ResponseEntity<>(wrongRequest, HttpStatus.NOT_FOUND);
    }

    //-------------------Bad Request Parameter Type --------------------------------------------------------
    @ExceptionHandler(TypeMismatchException.class)
    public ResponseEntity<WrongRequest> handleErrorMismatch(HttpServletRequest request, Exception e) {
        WrongRequest wrongRequest = new WrongRequest();
        wrongRequest.setRequest(request.getRequestURI());
        wrongRequest.setText("wrong request parameter");
        return new ResponseEntity<>(wrongRequest, HttpStatus.BAD_REQUEST);
    }

    //-------------------Bad Request OrderBy Parameter Type --------------------------------------------------------
    @ExceptionHandler(OrderResourceNotFoundException.class)
    public ResponseEntity<WrongRequest> handleErrorOrderByParam(
            HttpServletRequest request,
            Exception e
    ) {
        WrongRequest wrongRequest = new WrongRequest();
        wrongRequest.setRequest(e.getMessage());
        wrongRequest.setText("wrong orderBy parameter");
        return new ResponseEntity<>(wrongRequest, HttpStatus.BAD_REQUEST);
    }

    //-------------------Bad Request Content-Type--------------------------------------------------------
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public ResponseEntity<Void> handleErrorContentType(
            HttpServletRequest request,
            Exception e
    ) {
        return new ResponseEntity<>( HttpStatus.NOT_ACCEPTABLE);
    }
}