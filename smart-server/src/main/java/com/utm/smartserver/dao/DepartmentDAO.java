package com.utm.smartserver.dao;


import com.utm.smartserver.model.Department;
import com.utm.smartserver.model.Employee;

import java.util.List;

public interface DepartmentDAO {

    void saveDepartment(Department department);

    List<Department> findAllDepartments(Integer limit, String location, String name, String orderBy);

    void deleteDepartmentById(int id);

    Department findById(int id);

    void updateDepartment(Department department);
}
