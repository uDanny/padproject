package com.utm.smartserver.dao.impl;

import com.utm.smartserver.dao.AbstractDAO;
import com.utm.smartserver.dao.EmployeeDAO;
import com.utm.smartserver.model.Employee;
import com.utm.smartserver.model.Employee_;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.spi.LoadQueryInfluencers;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.internal.SessionImpl;
import org.hibernate.loader.OuterJoinLoader;
import org.hibernate.loader.criteria.CriteriaLoader;
import org.hibernate.persister.entity.OuterJoinLoadable;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Field;
import java.util.List;

@Repository("employeeDao")
public class EmployeeDAOImpl extends AbstractDAO implements EmployeeDAO {
    public void saveEmployee(Employee employee) {
        persist(employee);
    }

    @SuppressWarnings("unchecked")
    public List<Employee> findAllEmployees(Integer limit, Integer departmentId,
                                           String firstName, Integer maxSalary, String orderBy) {

        Criteria criteria = getSession().createCriteria(Employee.class);
        if (limit != null) {
            criteria.setMaxResults(limit);
        }

        if (departmentId != null) {
            criteria.add(Restrictions.eq("departmentId", departmentId));
        }
        if (firstName != null) {
            criteria.add(Restrictions.like("firstName",
                    "%" + firstName + "%"));
        }
        if (maxSalary != null) {
            criteria.add(Restrictions.lt("salary", maxSalary));
        }
        if (orderBy != null) {
            criteria.addOrder(Order.asc(orderBy.replace("_", "")));
        }

        return (List<Employee>) criteria.list();
    }

    @Override
    public void deleteEmployeeById(int id) {
        Query query = getSession().createSQLQuery("delete from Employee where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Employee findById(int id) {
        Criteria criteria = getSession().createCriteria(Employee.class);
        criteria.add(Restrictions.eq("id", id));
        return (Employee) criteria.uniqueResult();
    }

    @Override
    public void updateEmployee(Employee employee) {
        getSession().update(employee);
    }

}
