package com.utm.smartserver.dao.impl;

import com.utm.smartserver.dao.AbstractDAO;
import com.utm.smartserver.dao.DepartmentDAO;
import com.utm.smartserver.model.Department;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("departmentDao")
public class DepartmentDAOImpl extends AbstractDAO implements DepartmentDAO {


    @Override
    public void saveDepartment(Department department) {
        persist(department);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Department> findAllDepartments(Integer limit, String location, String name, String orderBy) {
        Criteria criteria = getSession().createCriteria(Department.class);

        if (limit != null) {
            criteria.setMaxResults(limit);
        }

        if (location != null) {
            criteria.add(Restrictions.eq("location", location));
        }
        if (name != null) {
            criteria.add(Restrictions.like("name",
                    "%" + name + "%"));
        }

        if (orderBy != null) {
            criteria.addOrder(Order.asc(orderBy));
        }

        return (List<Department>) criteria.list();
    }

    @Override
    public void deleteDepartmentById(int id) {
        Query query = getSession().createSQLQuery("delete from department where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Department findById(int id) {
        Criteria criteria = getSession().createCriteria(Department.class);
        criteria.add(Restrictions.eq("id", id));
        return (Department) criteria.uniqueResult();
    }

    @Override
    public void updateDepartment(Department department) {
        getSession().update(department);
    }
}
