package com.utm.smartserver.dao;


import com.utm.smartserver.model.Employee;

import java.util.List;

public interface EmployeeDAO {

    void saveEmployee(Employee employee);

    List<Employee> findAllEmployees(Integer limit, Integer departmentId, String firstName, Integer maxSalary, String orderBy);

    void deleteEmployeeById(int id);

    Employee findById(int id);

    void updateEmployee(Employee employee);
}
