package com.utm.smartserver.utils;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class Headers {
    public static MultiValueMap<String, String> getContentHeaders() {
        MultiValueMap<String, String> headersContent = new LinkedMultiValueMap<>();
        headersContent.add("Accept", "application/json");
        headersContent.add("Accept", "application/xml");
        headersContent.add("Content-Type", "application/json");
        headersContent.add("Content-Type", "application/xml");
        return headersContent;
    }

    public static MultiValueMap<String, String> getAcceptHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Accept", "application/json");
        headers.add("Accept", "application/xml");
        return headers;
    }
}
