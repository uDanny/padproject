package com.utm.smartserver.exception;

public class OrderResourceNotFoundException extends RuntimeException{
    public OrderResourceNotFoundException(String e) {
        super(e);
    }
}
