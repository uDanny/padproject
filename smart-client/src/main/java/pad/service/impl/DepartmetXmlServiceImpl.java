package pad.service.impl;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.RequestBodyEntity;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pad.model.Department;
import pad.service.DepartmentService;
import pad.utils.CriteriaParams;
import pad.utils.Port;

@Service("departmentXmlService")
@Transactional
public class DepartmetXmlServiceImpl extends Greeting implements DepartmentService {

    public static final String BASE_URI = "http://localhost:" + Port.getPort() + "/department/";

    @Override
    public HttpResponse<String> findById(int id) {
        HttpResponse<String> departmentResponse = null;
        try {
            departmentResponse = Unirest.get(BASE_URI + id)
                    .header("Accept", "application/xml")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return departmentResponse;
    }

    @Override
    public HttpResponse<JsonNode> saveDepartment(Department department) {

        HttpResponse<JsonNode> jsonResponse = null;
        RequestBodyEntity field = Unirest.post(BASE_URI)
                .header("Accept", "application/xml")
                .header("Content-Type", "application/xml")
                .body(getDepartmentXml(department));
        try {
            jsonResponse = field.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    private String getDepartmentXml(Department department) {
        return "<department>\n" +
                "    <location>" + department.getLocation() + "</location>\n" +
                "    <name>" + department.getName() + "</name>\n" +
                "</department>";
    }

    @Override
    public HttpResponse<String> updateDepartment(int id, Department department) {
        HttpResponse<String> jsonResponse = null;
        RequestBodyEntity field = Unirest.put(BASE_URI + id)
                .header("Accept", "application/xml")
                .header("Content-Type", "application/xml")
                .body(getDepartmentXml(department));
        try {
            jsonResponse = field.asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    @Override
    public HttpResponse<JsonNode> deleteDepartmentById(int id) {
        HttpResponse<JsonNode> httpResponse = null;
        HttpRequestWithBody delete = Unirest.delete(BASE_URI + id);

        try {
            httpResponse = delete.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return httpResponse;
    }

    @Override
    public HttpResponse<String> findAllDepartment(String limit, String location, String name, String orderBy) {
        String parameters = CriteriaParams.getDepartmentParameters(limit, location, name, orderBy);

        HttpResponse<String> employeeResponse = null;
        try {
            employeeResponse = Unirest.get(BASE_URI + parameters)
                    .header("Accept", "application/xml")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return employeeResponse;
    }

    @Override
    public HttpResponse<JsonNode> greeting() {
        return getGreeting(BASE_URI);
    }
}
