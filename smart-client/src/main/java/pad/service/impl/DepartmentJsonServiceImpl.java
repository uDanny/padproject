package pad.service.impl;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.RequestBodyEntity;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pad.model.Department;
import pad.service.DepartmentService;
import pad.utils.CriteriaParams;
import pad.utils.Port;

@Service("departmentJsonService")
@Transactional
public class DepartmentJsonServiceImpl extends Greeting implements DepartmentService {

    public static final String BASE_URI = "http://localhost:" + Port.getPort() + "/department/";

    @Override
    public HttpResponse findById(int id) {
        HttpResponse<JsonNode> departmentHttpResponse = null;
        GetRequest request = Unirest.get(BASE_URI + id);
        try {
            departmentHttpResponse = request
                    .header("Accept", "application/json")
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return departmentHttpResponse;
    }

    @Override
    public HttpResponse saveDepartment(Department department) {
        HttpResponse<JsonNode> jsonResponse = null;
        RequestBodyEntity field = Unirest.post(BASE_URI)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(getDepartmentJson(department));
        try {
            jsonResponse = field.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    private String getDepartmentJson(Department department) {
        return "{\n" +
                "    \"name\": \"" + department.getName() + "\",\n" +
                "    \"location\": \"" + department.getLocation() + "\"\n" +
                "}";
    }

    @Override
    public HttpResponse updateDepartment(int id, Department department) {
        HttpResponse<JsonNode> jsonResponse = null;
        RequestBodyEntity field = Unirest.put(BASE_URI + id)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(getDepartmentJson(department));
        try {
            jsonResponse = field.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    @Override
    public HttpResponse deleteDepartmentById(int id) {
        HttpResponse<JsonNode> httpResponse = null;
        HttpRequestWithBody delete = Unirest.delete(BASE_URI + id);

        try {
            httpResponse = delete.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return httpResponse;
    }

    @Override
    public HttpResponse findAllDepartment(String limit, String location, String name, String orderBy) {
        String parameters = CriteriaParams.getDepartmentParameters(limit, location, name, orderBy);

        HttpResponse<JsonNode> departmentsHttpResponse = null;
        GetRequest request = Unirest.get(BASE_URI + parameters);
        try {
            GetRequest accept = request.header("Accept", "application/json");

            departmentsHttpResponse = accept.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return departmentsHttpResponse;
    }

    @Override
    public HttpResponse<JsonNode> greeting() {
        return getGreeting(BASE_URI);
    }
}
