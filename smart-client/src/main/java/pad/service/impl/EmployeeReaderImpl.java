package pad.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

import org.json.JSONArray;
import org.json.JSONObject;
import pad.model.Employee;
import pad.service.EmployeeReader;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class EmployeeReaderImpl implements EmployeeReader {

    @Override
    public List<Employee> getJsonEmployeeList(HttpResponse<JsonNode> employeesHttpResponse) {

        List<Employee> employees = new LinkedList<>();

        JSONObject jsonObject = employeesHttpResponse.getBody().getObject();

        JSONArray jsonarray = jsonObject.getJSONArray("employees");

        for (int i = 0; i < jsonarray.length(); i++) {
            JSONObject jsObject = jsonarray.getJSONObject(i);
            Employee employee = getEmployee(jsObject);

            employees.add(employee);
        }

        return employees;
    }

    @Override
    public Employee getJsonEmployee(HttpResponse<JsonNode> employeeHttpResponse) {
        JSONObject jsonObject = employeeHttpResponse.getBody().getObject();

        Employee employee = getEmployee(jsonObject);

        return employee;
    }

    private Employee getEmployee(JSONObject jsonObject) {
        Employee employee = new Employee();
        employee.setId(jsonObject.getInt("id"));
        employee.setFirstName(jsonObject.getString("firstName"));
        employee.setLastName(jsonObject.getString("lastName"));
        employee.setDepartmentId(jsonObject.getInt("departmentId"));
        employee.setJobTitle(jsonObject.getString("jobTitle"));
        employee.setSalary(jsonObject.getInt("salary"));
        return employee;
    }

    @Override
    public Employee getXmlEmployee(HttpResponse<String> employeeResponse) {
        String xmlBody = employeeResponse.getBody();
        XmlMapper xmlMapper = new XmlMapper();
        Employee employee = null;
        try {
            employee = xmlMapper.readValue(xmlBody, Employee.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return employee;
    }

    @Override
    public List<Employee> getXmlEmployeeList(HttpResponse<String> employeeResponse) {
        String xmlBody = employeeResponse.getBody();
        XmlMapper xmlMapper = new XmlMapper();
        List<Employee> entries = null;
        try {
            entries = xmlMapper.readValue(xmlBody, new TypeReference<List<Employee>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return entries;
    }
}
