package pad.service.impl;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;

public class Greeting {
    HttpResponse<JsonNode> getGreeting(String URI){
        HttpResponse<JsonNode> HttpResponse = null;
        GetRequest request = Unirest.get(URI + "greeting");
        try {
            HttpResponse = request
                    .header("Accept", "application/json")
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return HttpResponse;
    }
}
