package pad.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import pad.model.Department;

import java.util.List;

public interface DepartmentReader {

    List<Department> getJsonDepartmentList(HttpResponse<JsonNode>departmentsHttpResponse);

    Department getJsonDepartment(HttpResponse<JsonNode> departmentsHttpResponse);

    Department getXmlDepartment(HttpResponse<String> departmentsResponse);

    List<Department> getXmlDepartmentList(HttpResponse<String> departmentsResponse);
}
