package pad.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import pad.model.Employee;


import java.util.List;

public interface EmployeeService<T> {


    HttpResponse<T> findById(int id);

    HttpResponse<T> saveEmployee(Employee employee);

    HttpResponse<T> updateEmployee(int id, Employee employee);

    HttpResponse<T> deleteEmployeeById(int id);

    HttpResponse<T> findAllEmployees(String limit, String departmentId, String firstName,
                                     String maxSalary, String orderBy);
    HttpResponse<JsonNode> greeting();
}
