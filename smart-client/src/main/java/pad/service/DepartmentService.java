package pad.service;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import pad.model.Department;

public interface DepartmentService<T> {


    HttpResponse<T> findById(int id);

    HttpResponse<T> saveDepartment(Department department);

    HttpResponse<T> updateDepartment(int id, Department department);

    HttpResponse<T> deleteDepartmentById(int id);

    HttpResponse<T> findAllDepartment(String limit, String location, String name, String orderBy);

    HttpResponse<JsonNode> greeting();


}
