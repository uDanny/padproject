package pad;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import org.json.JSONObject;
import pad.model.Department;
import pad.model.Employee;
import pad.service.DepartmentReader;
import pad.service.DepartmentService;
import pad.service.EmployeeReader;
import pad.service.EmployeeService;
import pad.service.impl.*;

import java.util.List;

public class Controller {
    public TextArea textarea;
    public ListView listview;
    public TextField limit ;
    public TextField departmentID ;
    public TextField fname ;
    public TextField maxsallary ;
    public TextField orderby ;
    public RadioButton jsonRadio;
    public RadioButton xmlRadio;
    final ToggleGroup format = new ToggleGroup();
    public Button getAllBtn;
    public Label responseStatus;
    public String varlimit;
    public String vardepartment;
    public String varfname;
    public String varmaxsallary;
    public String varorder;
    public String varlimitdep;
    public String varlocation;
    public String vardname;
    public String varorder1;
    public Button getOneBtn;
    public TextField idGetEmployee;
    public RadioButton jsonRadioGetOne;
    public RadioButton xmlRadioGetOne;
    final ToggleGroup formatGetOne = new ToggleGroup();
    public TextField fnameCreate;
    public TextField departCreate;
    public TextField lnameCreate;
    public TextField jtitleCreate;
    public TextField salCreate;
    public Button createbtn;
    public RadioButton xmlRadioCreate;
    public RadioButton jsonRadioCreate;
    final ToggleGroup formatCreate = new ToggleGroup();
    public RadioButton jsonRadioGetOne1;
    public TextField idGetEmployeeD;
    public Button deleteBtn;
    public RadioButton xmlRadioDelete;
    public RadioButton jsonRadioDelete;
    final ToggleGroup formatDelete = new ToggleGroup();
    public Button btnSearchEmployee;
    public TextField idSearchEmployee;
    public TextField fnameUpdate;
    public TextField departUpdate;
    public TextField lnameUpdate;
    public TextField jtitleUpdate;
    public TextField salUpdate;
    public Button Updatebtn;
    public RadioButton jsonRadioUpdate;
    public RadioButton xmlRadioUpdate;
    final ToggleGroup formatUpdate = new ToggleGroup();
    public Button hateos;
    public RadioButton jsonRadio1;
    public RadioButton xmlRadio1;
    final ToggleGroup formatgetalldep = new ToggleGroup();
    public TextField limit1;
    public TextField dname;
    public TextField locationDep;
    public TextField orderby1;
    public Button getAllBtn1;
    public EmployeeReader employeeReader = new EmployeeReaderImpl();
    public DepartmentReader departmentReader = new DepartmentReaderImpl() {
    };
    public RadioButton xmlRadioGetOne1;
    public Button getOneBtn1;
    public TextField idGetDep;
    public TextField idDepCreate;
    public TextField nameDepCreate;
    public TextField locationDepCreate;
    public Button createbtn1;
    public RadioButton jsonRadioCreate1;
    public RadioButton xmlRadioCreate1;
    public RadioButton jsonRadioDelete1;
    public TextField idGetDepartmentD;
    public RadioButton xmlRadioDelete1;
    public Button deleteBtn1;
    public Button btnSearchEmployee1;
    public TextField idSearchEmployee1;
    public TextField depIDUpdate;
    public TextField depLocationUpdate;
    public TextField depnameUpdate;
    public Button Updatebtn1;
    public RadioButton jsonRadioUpdate1;
    public RadioButton xmlRadioUpdate1;
    public Button hateosDep;
    final ToggleGroup formatGetOneDep = new ToggleGroup();
    final ToggleGroup formatCreateDep = new ToggleGroup();
    final ToggleGroup formatDeleteDep = new ToggleGroup();
    final ToggleGroup formatUpdateDep = new ToggleGroup();

    public void initialize(){
        jsonRadio.setToggleGroup(format);
        jsonRadio.setSelected(true);
        xmlRadio.setToggleGroup(format);

        jsonRadioGetOne.setToggleGroup(formatGetOne);
        jsonRadioGetOne.setSelected(true);
        xmlRadioGetOne.setToggleGroup(formatGetOne);

        jsonRadioCreate.setToggleGroup(formatCreate);
        jsonRadioCreate.setSelected(true);
        xmlRadioCreate.setToggleGroup(formatCreate);

        jsonRadioDelete.setToggleGroup(formatDelete);
        jsonRadioDelete.setSelected(true);
        xmlRadioDelete.setToggleGroup(formatDelete);

        jsonRadioUpdate.setToggleGroup(formatUpdate);
        jsonRadioUpdate.setSelected(true);
        xmlRadioUpdate.setToggleGroup(formatUpdate);

        jsonRadio1.setToggleGroup(formatgetalldep);
        jsonRadio1.setSelected(true);
        xmlRadio1.setToggleGroup(formatgetalldep);

        jsonRadioGetOne1.setToggleGroup(formatGetOneDep);
        jsonRadioGetOne1.setSelected(true);
        xmlRadioGetOne1.setToggleGroup(formatGetOneDep);

        jsonRadioCreate1.setToggleGroup(formatCreateDep);
        jsonRadioCreate1.setSelected(true);
        xmlRadioCreate1.setToggleGroup(formatCreateDep);

        jsonRadioDelete1.setToggleGroup(formatDeleteDep);
        jsonRadioDelete1.setSelected(true);
        xmlRadioDelete1.setToggleGroup(formatDeleteDep);

        jsonRadioUpdate1.setToggleGroup(formatUpdateDep);
        jsonRadioUpdate1.setSelected(true);
        xmlRadioUpdate1.setToggleGroup(formatUpdateDep);
    }
    public void showall(ActionEvent event){
        listview.getItems().clear();
        EmployeeService employeeService ;
        EmployeeReader employeeReader = new EmployeeReaderImpl();
        checkNULL();
        RadioButton selectedRadioButton = (RadioButton) format.getSelectedToggle();
        String toogleGroupValue = selectedRadioButton.getText();
        if(toogleGroupValue.equalsIgnoreCase("JSON")){

            employeeService = new EmployeeJsonServiceImpl();
            HttpResponse<JsonNode> httpResponse = employeeService.findAllEmployees(varlimit, vardepartment,varfname,varmaxsallary,varorder );
            responseStatus.setText(String.valueOf(httpResponse.getStatus()));
            textarea.setText("Response Status:" + httpResponse.getStatusText() +"\n" + httpResponse.getStatus() +"\n" + httpResponse.getHeaders()+"\n" + httpResponse.getBody());
            try{
                List<Employee> employeeList = employeeReader.getJsonEmployeeList(httpResponse);
                ObservableList<Employee> items = FXCollections.observableArrayList (employeeList);
                listview.setItems(items);
            }
            catch(Exception e){

            }
        }else{
            employeeService = new EmployeeXmlServiceImpl();
            HttpResponse<String> httpResponse = employeeService.findAllEmployees(varlimit, vardepartment,varfname,varmaxsallary,varorder );
            responseStatus.setText(httpResponse.getStatusText());
            textarea.setText(httpResponse.getStatusText()+"\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());
            try{
                List<Employee> employeeList = employeeReader.getXmlEmployeeList(httpResponse);
                ObservableList<Employee> items = FXCollections.observableArrayList (employeeList);
                listview.setItems(items);

            }
            catch(Exception e){

            }
        }


    }

    private void checkNULL() {

        if (limit.getText() == null || limit.getText().trim().isEmpty()) {
            varlimit = null;
        }else{
            varlimit = limit.getText();
          }

        if (departmentID.getText() == null || departmentID.getText().trim().isEmpty()) {
            vardepartment = null;
        }else{
            vardepartment = departmentID.getText();
        }

        if (fname.getText() == null || fname.getText().trim().isEmpty()) {
            varfname = null;
        }else{
            varfname = fname.getText();
        }

        if (maxsallary.getText() == null || maxsallary.getText().trim().isEmpty()) {
            varmaxsallary = null;
        }else{
            varmaxsallary = maxsallary.getText();
        }

        if (orderby.getText() == null || orderby.getText().trim().isEmpty()) {
            varorder = null;
        }else{
            varorder = orderby.getText();
        }
}

    public void showone(ActionEvent event){
        listview.getItems().clear();
        textarea.clear();
        EmployeeService employeeService ;
        int id = Integer.parseInt(idGetEmployee.getText());

        RadioButton selectedRadioButton = (RadioButton) formatGetOne.getSelectedToggle();
        String getOneRadioValue = selectedRadioButton.getText();
        if(getOneRadioValue.equalsIgnoreCase("JSON")){
            employeeService = new EmployeeJsonServiceImpl();
            HttpResponse<JsonNode> httpResponse = employeeService.findById(id);
            responseStatus.setText(String.valueOf(httpResponse.getStatus()));
            textarea.setText("Response Status:" + httpResponse.getStatusText() +"\n" + httpResponse.getStatus() +"\n" + httpResponse.getHeaders()+"\n" + httpResponse.getBody());
            try{
                Employee employee = employeeReader.getJsonEmployee(httpResponse);
                ObservableList<Employee> items = FXCollections.observableArrayList (employee);
                listview.setItems(items);

            }
            catch(Exception e){

            }
        }else{
            employeeService = new EmployeeXmlServiceImpl();
            HttpResponse<String> httpResponse = employeeService.findById(id);
            responseStatus.setText(httpResponse.getStatusText());
            textarea.setText(httpResponse.getStatusText()+"\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());
            try{
                Employee employee = employeeReader.getXmlEmployee(httpResponse);
                ObservableList<Employee> items = FXCollections.observableArrayList (employee);
                listview.setItems(items);

            }
            catch(Exception e){

            }
        }
    }
    @SuppressWarnings("Duplicates")
    public void createEmployee(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        RadioButton selectedRadioButton = (RadioButton) formatCreate.getSelectedToggle();
        String createRadioValue = selectedRadioButton.getText();
        EmployeeService employeeService;
        if(createRadioValue.equalsIgnoreCase("JSON")) {
             employeeService = new EmployeeJsonServiceImpl();
        }else{
             employeeService = new EmployeeXmlServiceImpl();
            }
        Employee employee = new Employee();
        createEmployeeObj(employee);
        HttpResponse<JsonNode> httpResponse = employeeService.saveEmployee(employee);
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        textarea.setText(httpResponse.getStatusText() + "\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());

    }
    @SuppressWarnings("Duplicates")
    private void createEmployeeObj(Employee employee) {
        employee.setFirstName(fnameCreate.getText());
        employee.setLastName(lnameCreate.getText());
        employee.setDepartmentId(Integer.parseInt(departCreate.getText()));
        employee.setJobTitle(jtitleCreate.getText());
        employee.setSalary(Integer.parseInt(salCreate.getText()));
    }
    @SuppressWarnings("Duplicates")
    private void updateEmployeeObj(Employee employee) {
        employee.setFirstName(fnameUpdate.getText());
        employee.setLastName(lnameUpdate.getText());
        employee.setDepartmentId(Integer.parseInt(departUpdate.getText()));
        employee.setJobTitle(jtitleUpdate.getText());
        employee.setSalary(Integer.parseInt(salUpdate.getText()));
    }
    @SuppressWarnings("Duplicates")
    public void deleteEmployee(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        int id = Integer.parseInt(idGetEmployeeD.getText());
        RadioButton selectedRadioButton = (RadioButton) formatDelete.getSelectedToggle();
        String deleteRadioValue = selectedRadioButton.getText();
        EmployeeService employeeService;
        if(deleteRadioValue.equalsIgnoreCase("JSON")) {
            employeeService = new EmployeeJsonServiceImpl();
        }else{
            employeeService = new EmployeeXmlServiceImpl();
        }
        HttpResponse<JsonNode> httpResponse = employeeService.deleteEmployeeById(id);
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        textarea.setText(httpResponse.getStatusText() + "\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());

    }
    @SuppressWarnings("Duplicates")
    public void UpdateEmployee(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        RadioButton selectedRadioButton = (RadioButton) formatUpdate.getSelectedToggle();
        String updateRadioValue = selectedRadioButton.getText();
        EmployeeService employeeService;
        if(updateRadioValue.equalsIgnoreCase("JSON")) {
            employeeService = new EmployeeJsonServiceImpl();
        }else{
            employeeService = new EmployeeXmlServiceImpl();
        }
        Employee employee = new Employee();
        updateEmployeeObj(employee);
        Integer employeeid = Integer.parseInt(idSearchEmployee.getText());
        HttpResponse<JsonNode> httpResponse = employeeService.updateEmployee(employeeid,employee);
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        textarea.setText(httpResponse.getStatusText() + "\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());

    }

    public void SearchEmployee(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        EmployeeService employeeService ;
        int id = Integer.parseInt(idSearchEmployee.getText());
        employeeService = new EmployeeJsonServiceImpl();
        HttpResponse<JsonNode> httpResponse = employeeService.findById(id);
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        Employee employee = employeeReader.getJsonEmployee(httpResponse);
        getEmployeeObj(employee);
    }

    private void getEmployeeObj(Employee employee) {
        fnameUpdate.setText(employee.getFirstName());
        lnameUpdate.setText(employee.getLastName());
        departUpdate.setText(String.valueOf(employee.getDepartmentId()));
        jtitleUpdate.setText(employee.getJobTitle());
        salUpdate.setText(String.valueOf(employee.getSalary()));
    }

    public void greeting(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        EmployeeService employeeService = new EmployeeJsonServiceImpl();
        HttpResponse<JsonNode> httpResponse = employeeService.greeting();
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        textarea.setText(httpResponse.getStatusText() + "\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());
        JSONObject jsonObject = httpResponse.getBody().getObject();


      //  ObservableList<JSONObject> items = FXCollections.observableArrayList (jsonObject);
        //listview.setItems(items);
    }
    @SuppressWarnings("Duplicates")
    public void showallDep(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        DepartmentService departmentService ;
        checkNullDepCriteria();
        RadioButton selectedRadioButton = (RadioButton) formatgetalldep.getSelectedToggle();
        String toogleGroupValueDep = selectedRadioButton.getText();
        if(toogleGroupValueDep.equalsIgnoreCase("JSON")){

            departmentService = new DepartmentJsonServiceImpl();
            HttpResponse<JsonNode> httpResponse = departmentService.findAllDepartment(varlimitdep, varlocation,vardname,varorder1 );
            responseStatus.setText(String.valueOf(httpResponse.getStatus()));
            textarea.setText("Response Status:" + httpResponse.getStatusText() +"\n" + httpResponse.getStatus() +"\n" + httpResponse.getHeaders()+"\n" + httpResponse.getBody());
            try{
                List<Department> departmentList = departmentReader.getJsonDepartmentList(httpResponse);
                ObservableList<Department> items = FXCollections.observableArrayList (departmentList);
                listview.setItems(items);
            }
            catch(Exception e){

            }
        }else{
            departmentService = new DepartmetXmlServiceImpl();
            HttpResponse<String> httpResponse = departmentService.findAllDepartment(varlimitdep, varlocation,vardname,varorder1 );
            responseStatus.setText(httpResponse.getStatusText());
            textarea.setText(httpResponse.getStatusText()+"\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());
            try{
                List<Department> departmentList = departmentReader.getXmlDepartmentList(httpResponse);
                ObservableList<Department> items = FXCollections.observableArrayList (departmentList);
                listview.setItems(items);
            }
            catch(Exception e){

            }
        }


    }
    @SuppressWarnings("Duplicates")
    public void showOneDep(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        DepartmentService departmentService ;
        int idDep = Integer.parseInt(idGetDep.getText());
        RadioButton selectedRadioButton = (RadioButton) formatGetOneDep.getSelectedToggle();
        String toogleGroupValueDep = selectedRadioButton.getText();
        if(toogleGroupValueDep.equalsIgnoreCase("JSON")){

            departmentService = new DepartmentJsonServiceImpl();
            HttpResponse<JsonNode> httpResponse = departmentService.findById(idDep);
            responseStatus.setText(String.valueOf(httpResponse.getStatus()));
            textarea.setText("Response Status:" + httpResponse.getStatusText() +"\n" + httpResponse.getStatus() +"\n" + httpResponse.getHeaders()+"\n" + httpResponse.getBody());
            try{
                Department department = departmentReader.getJsonDepartment(httpResponse);
                ObservableList<Department> items = FXCollections.observableArrayList (department);
                listview.setItems(items);
            }
            catch(Exception e){

            }
        }else{
            departmentService = new DepartmetXmlServiceImpl();
            HttpResponse<String> httpResponse = departmentService.findAllDepartment(varlimitdep, varlocation,vardname,varorder1 );
            responseStatus.setText(httpResponse.getStatusText());
            textarea.setText(httpResponse.getStatusText()+"\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());
            try{
                Department department = departmentReader.getXmlDepartment(httpResponse);
                ObservableList<Department> items = FXCollections.observableArrayList (department);
                listview.setItems(items);
            }
            catch(Exception e){

            }
        }
    }
    @SuppressWarnings("Duplicates")
    public void createDepartment(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        RadioButton selectedRadioButton = (RadioButton) formatCreateDep.getSelectedToggle();
        String createRadioValuedep = selectedRadioButton.getText();
        DepartmentService departmentService;
        if(createRadioValuedep.equalsIgnoreCase("JSON")) {
            departmentService = new DepartmentJsonServiceImpl();
        }else{
            departmentService = new DepartmetXmlServiceImpl();
        }
        Department department = new Department();
        createDeparmentObj(department);
        HttpResponse<JsonNode> httpResponse = departmentService.saveDepartment(department);
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        textarea.setText(httpResponse.getStatusText() + "\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());

    }

    public void deleteDepartment(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        int id = Integer.parseInt(idGetDepartmentD.getText());
        RadioButton selectedRadioButton = (RadioButton) formatDeleteDep.getSelectedToggle();
        String deleteRadioValue = selectedRadioButton.getText();
        DepartmentService departmentService;
        if(deleteRadioValue.equalsIgnoreCase("JSON")) {
            departmentService = new DepartmentJsonServiceImpl();
        }else{
            departmentService = new DepartmetXmlServiceImpl();
        }
        HttpResponse<JsonNode> httpResponse = departmentService.deleteDepartmentById(id);
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        textarea.setText(httpResponse.getStatusText() + "\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());

    }
    @SuppressWarnings("Duplicates")
    public void UpdateDepartment(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        RadioButton selectedRadioButtonDep = (RadioButton) formatUpdateDep.getSelectedToggle();
        String updateRadioValueDep = selectedRadioButtonDep.getText();
        DepartmentService departmentService;
        if(updateRadioValueDep.equalsIgnoreCase("JSON")) {
            departmentService = new DepartmentJsonServiceImpl();
        }else{
            departmentService = new DepartmetXmlServiceImpl();
        }
        Department department = new Department();
        updateDeparmentObj(department);
        Integer deparmentid = Integer.parseInt(idSearchEmployee1.getText());
        HttpResponse<JsonNode> httpResponse = departmentService.updateDepartment(deparmentid,department);
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        textarea.setText(httpResponse.getStatusText() + "\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());

    }

    public void greetingDep(ActionEvent actionEvent) {
        listview.getItems().clear();
        textarea.clear();
        DepartmentService departmentService = new DepartmentJsonServiceImpl();
        HttpResponse<JsonNode> httpResponse = departmentService.greeting();
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        textarea.setText(httpResponse.getStatusText() + "\n" + httpResponse.getStatus() + httpResponse.getHeaders() + httpResponse.getBody());
        JSONObject jsonObject = httpResponse.getBody().getObject();


      //  ObservableList<JSONObject> items = FXCollections.observableArrayList (jsonObject);
       // listview.setItems(items);
    }
    private void checkNullDepCriteria() {

        if (limit1.getText() == null || limit1.getText().trim().isEmpty()) {
            varlimitdep  = null;
        }else{
            varlimitdep = limit1.getText();
        }

        if (locationDep.getText() == null || locationDep.getText().trim().isEmpty()) {
            varlocation = null;
        }else{
            varlocation = locationDep.getText();
        }

        if (dname.getText() == null || dname.getText().trim().isEmpty()) {
            vardname = null;
        }else{
            vardname = dname.getText();
        }


        if (orderby1.getText() == null || orderby1.getText().trim().isEmpty()) {
            varorder1 = null;
        }else{
            varorder1 = orderby1.getText();
        }
    }
    @SuppressWarnings("Duplicates")
    private void createDeparmentObj(Department department) {
        department.setName(nameDepCreate.getText());
        department.setLocation(locationDepCreate.getText());

    }
    private void updateDeparmentObj(Department department) {
        department.setName(depnameUpdate.getText());
        department.setLocation(depLocationUpdate.getText());
    }
    @SuppressWarnings("Duplicates")
    public void SearchDepartment(ActionEvent actionEvent) {
        DepartmentService departmentService ;
        int id = Integer.parseInt(idSearchEmployee1.getText());
        departmentService = new DepartmentJsonServiceImpl();
        HttpResponse<JsonNode> httpResponse = departmentService.findById(id);
        responseStatus.setText(String.valueOf(httpResponse.getStatus()));
        Department department = departmentReader.getJsonDepartment(httpResponse);
        getDepartmentObj(department);
    }
    private void getDepartmentObj(Department department) {
        depnameUpdate.setText(department.getName());
        depLocationUpdate.setText(department.getLocation());
    }
}
