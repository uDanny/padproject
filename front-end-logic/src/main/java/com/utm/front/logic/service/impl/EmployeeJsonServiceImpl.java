package com.utm.front.logic.service.impl;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.utm.front.logic.model.Employee;
import com.utm.front.logic.service.EmployeeService;
import com.utm.front.logic.utils.CriteriaParams;
import com.utm.front.logic.utils.Port;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("employeeJsonService")
@Transactional
public class EmployeeJsonServiceImpl extends Greeting implements EmployeeService {

    public static final String BASE_URI = "http://localhost:" + Port.getPort() + "/employee/";

    @Override
    public HttpResponse<JsonNode> findById(int id) {
        HttpResponse<JsonNode> employeeHttpResponse = null;
        GetRequest request = Unirest.get(BASE_URI + id);
        try {
            employeeHttpResponse = request
                    .header("Accept", "application/json")
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return employeeHttpResponse;
    }

    @Override
    public HttpResponse<JsonNode> saveEmployee(Employee employee) {

        HttpResponse<JsonNode> jsonResponse = null;
        RequestBodyEntity field = Unirest.post(BASE_URI)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(getEmployeeJson(employee));
        try {
            jsonResponse = field.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonResponse;

    }

    @Override
    public HttpResponse<JsonNode> updateEmployee(int id, Employee employee) {

        HttpResponse<JsonNode> jsonResponse = null;
        RequestBodyEntity field = Unirest.put(BASE_URI + id)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(getEmployeeJson(employee));
        try {
            jsonResponse = field.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    private String getEmployeeJson(Employee employee) {
        return "{\n" +
                "    \"firstName\": \"" + employee.getFirstName() + "\",\n" +
                "    \"lastName\": \"" + employee.getLastName() + "\",\n" +
                "    \"departmentId\": " + employee.getDepartmentId() + ",\n" +
                "    \"jobTitle\": \"" + employee.getJobTitle() + "\",\n" +
                "    \"salary\": " + employee.getSalary() + "\n" +
                "}";
    }


    @Override
    public HttpResponse<JsonNode> deleteEmployeeById(int id) {
        HttpResponse<JsonNode> httpResponse = null;
        HttpRequestWithBody delete = Unirest.delete(BASE_URI + id);

        try {
            httpResponse = delete.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return httpResponse;
    }

    @Override
    public HttpResponse<JsonNode> findAllEmployees(Integer limit, Integer departmentId, String firstName,
                                                   Integer maxSalary, String orderBy) {
        String parameters = CriteriaParams.getEmployeeParameters(limit, departmentId, firstName, maxSalary, orderBy);

        HttpResponse<JsonNode> employeesHttpResponse = null;
        GetRequest request = Unirest.get(BASE_URI + parameters);
        try {
            GetRequest accept = request.header("Accept", "application/json");

            employeesHttpResponse = accept.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return employeesHttpResponse;

    }

    @Override
    public HttpResponse<JsonNode> greeting() {
            return getGreeting(BASE_URI);
    }


}
