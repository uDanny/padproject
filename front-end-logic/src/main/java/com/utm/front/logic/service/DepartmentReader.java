package com.utm.front.logic.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Department;
import com.utm.front.logic.model.Employee;

import java.util.List;

public interface DepartmentReader {

    List<Department> getJsonDepartmentList(HttpResponse<JsonNode>departmentsHttpResponse);

    Department getJsonDepartment(HttpResponse<JsonNode> departmentsHttpResponse);

    Department getXmlDepartment(HttpResponse<String> departmentsResponse);

    List<Department> getXmlDepartmentList(HttpResponse<String> departmentsResponse);
}
