package com.utm.front.logic.service;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Department;
import com.utm.front.logic.model.Employee;

public interface DepartmentService<T> {


    HttpResponse<T> findById(int id);

    HttpResponse<T> saveDepartment(Department department);

    HttpResponse<T> updateDepartment(int id, Department department);

    HttpResponse<T> deleteDepartmentById(int id);

    HttpResponse<T> findAllDepartment(Integer limit, String location, String name, String orderBy);

    HttpResponse<JsonNode> greeting();


}
