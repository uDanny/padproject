package com.utm.front.logic.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Employee;
import org.json.JSONObject;

import java.util.List;

public interface EmployeeReader {

    List<Employee> getJsonEmployeeList(HttpResponse<JsonNode> employeesHttpResponse);

    Employee getJsonEmployee(HttpResponse<JsonNode> employeeHttpResponse);

    Employee getXmlEmployee(HttpResponse<String> employeeResponse);

    List<Employee> getXmlEmployeeList(HttpResponse<String> employeeResponse);
}
