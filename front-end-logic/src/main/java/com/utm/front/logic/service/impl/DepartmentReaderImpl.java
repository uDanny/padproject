package com.utm.front.logic.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Department;
import com.utm.front.logic.model.Employee;
import com.utm.front.logic.model.Employees;
import com.utm.front.logic.service.DepartmentReader;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class DepartmentReaderImpl implements DepartmentReader {
    @Override
    public List<Department> getJsonDepartmentList(HttpResponse<JsonNode> departmentsHttpResponse) {
        List<Department> departments = new LinkedList<>();

        JSONObject jsonObject = departmentsHttpResponse.getBody().getObject();

        JSONArray jsonarray = jsonObject.getJSONArray("departments");

        for (int i = 0; i < jsonarray.length(); i++) {
            JSONObject jsObject = jsonarray.getJSONObject(i);
            Department department = getDepartmentFromJson(jsObject);
            departments.add(department);
        }

        return departments;
    }

    @Override
    public Department getJsonDepartment(HttpResponse<JsonNode> departmentsHttpResponse) {
        JSONObject jsonObject = departmentsHttpResponse.getBody().getObject();

        Department department = getDepartmentFromJson(jsonObject);


        return department;
    }

    private Department getDepartmentFromJson(JSONObject jsonObject) {
        Department department = new Department();
        department.setId(jsonObject.getInt("id"));
        department.setLocation(jsonObject.getString("name"));
        department.setName(jsonObject.getString("location"));
        return department;
    }

    @Override
    public Department getXmlDepartment(HttpResponse<String> departmentsResponse) {
        String xmlBody = departmentsResponse.getBody();
        XmlMapper xmlMapper = new XmlMapper();
        Department department = null;
        try {
            department = xmlMapper.readValue(xmlBody, Department.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return department;
    }

    @Override
    public List<Department> getXmlDepartmentList(HttpResponse<String> departmentsResponse) {
        String xmlBody = departmentsResponse.getBody();
        XmlMapper xmlMapper = new XmlMapper();
        List<Department> entries = null;
        try {
            entries = xmlMapper.readValue(xmlBody, new TypeReference<List<Department>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return entries;
    }
}
