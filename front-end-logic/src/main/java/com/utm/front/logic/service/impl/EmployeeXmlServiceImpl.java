package com.utm.front.logic.service.impl;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.utm.front.logic.model.Employee;
import com.utm.front.logic.service.EmployeeService;
import com.utm.front.logic.utils.CriteriaParams;
import com.utm.front.logic.utils.Port;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("employeeXmlService")
@Transactional
public class EmployeeXmlServiceImpl extends Greeting implements EmployeeService {

    public static final String BASE_URI = "http://localhost:" + Port.getPort() + "/employee/";

    @Override
    public HttpResponse<String> findById(int id) {
        HttpResponse<String> employeeResponse = null;
        try {
            employeeResponse = Unirest.get(BASE_URI + id)
                    .header("Accept", "application/xml")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return employeeResponse;
    }

    @Override
    public HttpResponse<JsonNode> saveEmployee(Employee employee) {

        HttpResponse<JsonNode> jsonResponse = null;
        RequestBodyEntity field = Unirest.post(BASE_URI)
                .header("Accept", "application/xml")
                .header("Content-Type", "application/xml")
                .body(getEmployeeXml(employee));
        try {
            jsonResponse = field.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonResponse;

    }
    @Override
    public HttpResponse<String> updateEmployee(int id, Employee employee) {

        HttpResponse<String> jsonResponse = null;
        RequestBodyEntity field = Unirest.put(BASE_URI + id)
                .header("Accept", "application/xml")
                .header("Content-Type", "application/xml")
                .body(getEmployeeXml(employee));
        try {
            jsonResponse = field.asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    private String getEmployeeXml(Employee employee) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<employee>\n" +
                "    <departmentId>" + employee.getDepartmentId() + "</departmentId>\n" +
                "    <firstName>" + employee.getFirstName() + "</firstName>\n" +
                "    <jobTitle>" + employee.getJobTitle() + "</jobTitle>\n" +
                "    <lastName>" + employee.getLastName() + "</lastName>\n" +
                "    <salary>" + employee.getSalary() + "</salary>\n" +
                "</employee>";
    }



    @Override
    public HttpResponse<JsonNode> deleteEmployeeById(int id) {
        HttpResponse<JsonNode> httpResponse = null;
        HttpRequestWithBody delete = Unirest.delete(BASE_URI + id);

        try {
            httpResponse = delete.asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return httpResponse;
    }

    @Override
    public HttpResponse<String> findAllEmployees(Integer limit, Integer departmentId, String firstName,
                                                 Integer maxSalary, String orderBy) {
        String parameters = CriteriaParams.getEmployeeParameters(limit, departmentId, firstName, maxSalary, orderBy);

        HttpResponse<String> employeeResponse = null;
        try {
            employeeResponse = Unirest.get(BASE_URI + parameters)
                    .header("Accept", "application/xml")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return employeeResponse;
    }

    @Override
    public HttpResponse<JsonNode> greeting() {
            return getGreeting(BASE_URI);
    }


}
