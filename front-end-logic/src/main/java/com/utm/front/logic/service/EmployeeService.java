package com.utm.front.logic.service;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Employee;

import java.util.List;

public interface EmployeeService<T> {


    HttpResponse<T> findById(int id);

    HttpResponse<T> saveEmployee(Employee employee);

    HttpResponse<T> updateEmployee(int id, Employee employee);

    HttpResponse<T> deleteEmployeeById(int id);

    HttpResponse<T> findAllEmployees(Integer limit, Integer departmentId, String firstName,
                                            Integer maxSalary, String orderBy);
    HttpResponse<JsonNode> greeting();
}
