package com.utm.front.logic.utils;

import java.util.LinkedList;
import java.util.List;

public class CriteriaParams {

    public static String getEmployeeParameters(Integer limit, Integer departmentId, String firstName, Integer maxSalary, String orderBy) {
        List<String> parameters = new LinkedList<>();
        if (limit != null) {
            parameters.add("limit=" + limit);
        }
        if (departmentId != null) {
            parameters.add("department_id=" + departmentId);
        }
        if (firstName != null) {
            parameters.add("contains_firstname=" + firstName);
        }
        if (maxSalary != null) {
            parameters.add("max_salary=" + maxSalary);
        }
        if (orderBy != null) {
            parameters.add("order_by=" + orderBy);
        }
        StringBuilder stringBuilder = new StringBuilder("");

        if (!parameters.isEmpty()) {
            for (int i = 0; i < parameters.size(); i++) {
                if (i == 0) {
                    stringBuilder.append("?");
                } else {
                    stringBuilder.append("&");
                }
                stringBuilder.append(parameters.get(i));
            }
        }
        return stringBuilder.toString();
    }



    public static String getDepartmentParameters(Integer limit, String location, String name, String orderBy) {
        List<String> parameters = new LinkedList<>();
        if (limit != null) {
            parameters.add("limit=" + limit);
        }
        if (location != null) {
            parameters.add("location=" + location);
        }
        if (name != null) {
            parameters.add("name=" + name);
        }
        if (orderBy != null) {
            parameters.add("orderBy=" + orderBy);
        }

        StringBuilder stringBuilder = new StringBuilder("");

        if (!parameters.isEmpty()) {
            for (int i = 0; i < parameters.size(); i++) {
                if (i == 0) {
                    stringBuilder.append("?");
                } else {
                    stringBuilder.append("&");
                }
                stringBuilder.append(parameters.get(i));
            }
        }
        return stringBuilder.toString();
    }
}
