package com.utm.front.logic.service.impl;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Employee;
import com.utm.front.logic.service.EmployeeReader;
import com.utm.front.logic.service.EmployeeService;
import org.junit.jupiter.api.Test;

import java.util.List;

public class EmployeeJsonServiceImplTest {

    EmployeeService employeeService = new EmployeeJsonServiceImpl();
    EmployeeReader employeeReader = new EmployeeReaderImpl();

    @Test
    void findAllEmployees() {
        HttpResponse<JsonNode> httpResponse = employeeService.findAllEmployees(1, 1, "a", 2, "id");
        List<Employee> employeeList = employeeReader.getJsonEmployeeList(httpResponse);

        System.out.println(employeeList.toString());
    }

    @Test
    void findEmployee() {
        HttpResponse<JsonNode> httpResponse = employeeService.findById(1);
        Employee employee = employeeReader.getJsonEmployee(httpResponse);

        System.out.println(employee.toString());
    }

    @Test
    void deleteEmployee() {
        HttpResponse<JsonNode> httpResponse = employeeService.deleteEmployeeById(1);

    }

    @Test
    void createEmployee() {
        Employee employee = getEmployee();
        HttpResponse<JsonNode> httpResponse = employeeService.saveEmployee(employee);

    }

    @Test
    void updateEmployee() {
        Employee employee = getEmployee();
        HttpResponse<JsonNode> httpResponse = employeeService.updateEmployee(2, employee);

        Employee jsonEmployee = employeeReader.getJsonEmployee(httpResponse);
        System.out.println(jsonEmployee.toString());

    }
    @Test
    void greetongTest() {
        HttpResponse greeting = employeeService.greeting();
        System.out.println(greeting.getBody().toString());
    }

    private Employee getEmployee() {
        Employee employee = new Employee();
        employee.setId(0);
        employee.setFirstName("fn");
        employee.setLastName("ln");
        employee.setJobTitle("jb");
        employee.setSalary(100);
        employee.setDepartmentId(2);
        return employee;
    }
}