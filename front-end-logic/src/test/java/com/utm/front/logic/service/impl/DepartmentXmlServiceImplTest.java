package com.utm.front.logic.service.impl;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Department;
import com.utm.front.logic.model.Employee;
import com.utm.front.logic.service.DepartmentReader;
import com.utm.front.logic.service.DepartmentService;
import org.junit.jupiter.api.Test;

import java.util.List;

public class DepartmentXmlServiceImplTest {

    DepartmentService departmentService = new DepartmetXmlServiceImpl();
    DepartmentReader departmentReader = new DepartmentReaderImpl();

    @Test
    void findAllEmployees() {
        HttpResponse<String> httpResponse = departmentService.findAllDepartment(2, "aa", "o", "id");
        List<Department> departmentList = departmentReader.getXmlDepartmentList(httpResponse);

        System.out.println(departmentList.toString());
    }

    @Test
    void findEmployee() {
        HttpResponse<String> httpResponse = departmentService.findById(2);
        Department department = departmentReader.getXmlDepartment(httpResponse);

        System.out.println(department.toString());
    }

    @Test
    void deleteEmployee() {
        HttpResponse<JsonNode> httpResponse = departmentService.deleteDepartmentById(16);

    }

    @Test
    void createEmployee() {
        Department department = getDepartment();
        HttpResponse<JsonNode> httpResponse = departmentService.saveDepartment(department);
        System.out.println(httpResponse.getBody().toString());

    }

    @Test
    void updateEmployee() {
        Department department = getDepartment();
        HttpResponse<String> httpResponse = departmentService.updateDepartment(12, department);

        Department xmlDepartment = departmentReader.getXmlDepartment(httpResponse);
        System.out.println(xmlDepartment.toString());

    }

    @Test
    void greetongTest() {
        HttpResponse greeting = departmentService.greeting();
        System.out.println(greeting.getBody().toString());
    }

    private Department getDepartment() {
        Department department = new Department();
        department.setId(0);
        department.setLocation("xmlLocation");
        department.setName("xmlName");
        return department;
    }
}