package com.utm.front.logic.service.impl;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Employee;
import com.utm.front.logic.service.EmployeeReader;
import com.utm.front.logic.service.EmployeeService;
import org.junit.jupiter.api.Test;

import java.util.List;

public class EmployeeXmlServiceImplTest {

    EmployeeService employeeService = new EmployeeXmlServiceImpl();
    EmployeeReader employeeReader = new EmployeeReaderImpl();

    @Test
    void findAllEmployees() {
        HttpResponse<String> httpResponse = employeeService.findAllEmployees(2, null, null, null, null);
        List<Employee> employeeList = employeeReader.getXmlEmployeeList(httpResponse);

        System.out.println(employeeList.toString());
    }

    @Test
    void findEmployee() {
        HttpResponse<String> httpResponse = employeeService.findById(2);
        Employee employee = employeeReader.getXmlEmployee(httpResponse);

        System.out.println(employee.toString());
    }

    @Test
    void deleteEmployee() {
        HttpResponse<JsonNode> httpResponse = employeeService.deleteEmployeeById(1);

    }

    @Test
    void createEmployee() {
        Employee employee = getEmployee();
        HttpResponse<JsonNode> httpResponse = employeeService.saveEmployee(employee);
        System.out.println(httpResponse.getBody().toString());

    }

    @Test
    void updateEmployee() {
        Employee employee = getEmployee();
        HttpResponse<String> httpResponse = employeeService.updateEmployee(2, employee);

        Employee xmlEmployee = employeeReader.getXmlEmployee(httpResponse);
        System.out.println(xmlEmployee.toString());

    }

    @Test
    void greetongTest() {
        HttpResponse greeting = employeeService.greeting();
        System.out.println(greeting.getBody().toString());
    }

    private Employee getEmployee() {
        Employee employee = new Employee();
        employee.setId(0);
        employee.setFirstName("fn000");
        employee.setLastName("ln");
        employee.setJobTitle("jb");
        employee.setSalary(100);
        employee.setDepartmentId(2);
        return employee;
    }
}