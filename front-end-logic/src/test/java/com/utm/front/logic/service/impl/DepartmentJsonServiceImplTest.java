package com.utm.front.logic.service.impl;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.utm.front.logic.model.Department;
import com.utm.front.logic.model.Employee;
import com.utm.front.logic.service.DepartmentReader;
import com.utm.front.logic.service.DepartmentService;
import org.junit.jupiter.api.Test;

import java.util.List;

public class DepartmentJsonServiceImplTest {

    DepartmentService departmentService = new DepartmentJsonServiceImpl();
    DepartmentReader departmentReader = new DepartmentReaderImpl();

    @Test
    void findAllDepartments() {
        HttpResponse<JsonNode> httpResponse = departmentService.findAllDepartment(null, null, null, null);
        List<Department> departmentList = departmentReader.getJsonDepartmentList(httpResponse);

        System.out.println(departmentList.toString());
    }

    @Test
    void findDepartment() {
        HttpResponse<JsonNode> httpResponse = departmentService.findById(2);
        Department department = departmentReader.getJsonDepartment(httpResponse);

        System.out.println(department.toString());
    }

    @Test
    void deleteDepartment() {
        HttpResponse<JsonNode> httpResponse = departmentService.deleteDepartmentById(14);

    }

    @Test
    void createDepartment() {
        Department department = getDepartment();
        HttpResponse<JsonNode> httpResponse = departmentService.saveDepartment(department);

    }

    @Test
    void updateDepartment() {
        Department department = getDepartment();
        HttpResponse<JsonNode> httpResponse = departmentService.updateDepartment(15, department);

        Department department1 = departmentReader.getJsonDepartment(httpResponse);
        System.out.println(department1.toString());

    }

    @Test
    void greetongTest() {
        HttpResponse greeting = departmentService.greeting();
        System.out.println(greeting.getBody().toString());
    }

    private Department getDepartment() {
        Department department = new Department();
        department.setId(0);
        department.setName("fnDep");
        department.setLocation("lnDep");
        return department;
    }
}